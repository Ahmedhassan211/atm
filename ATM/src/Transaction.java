import java.util.Date;

public class Transaction {

	private double amount;
	private Date timestape;
	//Deposit reference.
	private String reference;
	//Stores account that made the transaction
	private Account inAccount;
	
	public Transaction(double amount, Account inAccount) {
		this.amount = amount;
		this.inAccount = inAccount;
		this.reference = "";
		this.timestape = new Date();
	}
	//java will decide which arguement to use based on variables required.
	public Transaction (double amount, Account inAccount, String reference) {
		//constructor first.
		this(amount,inAccount);
		this.reference = reference;
		
	}
}

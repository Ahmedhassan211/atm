import java.util.ArrayList;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class User {

	private String firstName;
	private String lastName;
	private String id;
	//Hash the users pin number
	private byte pinHash[];
	//May have multiple accounts
	private ArrayList<Account> accounts;
	
	public User(String firstName, String lastName, String pin, Bank theBank){
		//setting name
		this.firstName = firstName;
		this.lastName = lastName;
		
		//hashing pin using MD5 common for passwords/pins
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			this.pinHash = md.digest(pin.getBytes());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();
			System.exit(1);
		}
		
		//getting a new id
		this.id = theBank.getNewUserID();
		
		//List of accounts
		this.accounts = new ArrayList<Account>();
		
		//print log message
		System.out.printf("New user %s, %s with ID %s created. \n", lastName, firstName, this.id);
		
	}
	
	public void addAccount(Account anAcct) {
		this.accounts.add(anAcct);
	}
	public String getid() {
		return this.id;
	}
	public boolean validatePin(String aPin) {
		
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			this.pinHash = md.digest(pin.getBytes());
			return MessageDigest.isEqual(md.digest(aPin.getBytes()), this.pinHash)
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			System.err.println("NoSuchAlgorithmException");
			e.printStackTrace();
			System.exit(1);
		}
		
		return false;
	}
	
	
	
}

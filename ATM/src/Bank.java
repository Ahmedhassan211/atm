import java.util.ArrayList;
import java.util.Random;

public class Bank {

	private String name;
	private ArrayList<User> users;
	private ArrayList<Account> accounts;
	public Bank(String name) {
		this.name = name;
		this.users = new ArrayList<Users>();
		this.accounts = new ArrayList<Account>();
		
	}
	
	public String getNewUserID() {
		
		//Generating unique ID
		String id;
		Random rnd = new Random();
		int length = 6;
		boolean nonUnique;
		
		//Keep generating until ID is unique
		do {
			id = "";
			for(int i= 0; i<length; i++) {
				//Adding a number between 1-9 based on the length.
				id +=((Integer)rnd.nextInt(10)).toString();
			}
			
			//Check to see if its unique
			nonUnique = false;
			for(User u : this.users) {
				if(id.compareTo(u.getid()) == 0) {
					nonUnique = true;
					break;
				}
			}
		}while (nonUnique);		
		return id;
		
	}
	
	public String getNewAccountID() {
		
//Generating unique ID
		String id;
		Random rnd = new Random();
		int length = 10;
		boolean nonUnique;
		
		//Keep generating until ID is unique
		do {
			id = "";
			for(int i= 0; i<length; i++) {
				//Adding a number between 1-9 based on the length.
				id +=((Integer)rnd.nextInt(10)).toString();
			}
			
			//Check to see if its unique
				nonUnique = false;
				for(Account a : this.accounts) {
					if(id.compareTo(a.getid()) == 0) {
						nonUnique = true;
						break;
					}
				}
			}while (nonUnique);		
			return id;
			
		}
	
	
	public void addAccount(Account anAcct) {
		this.accounts.add(anAcct);
	}
	public User addUser(String firstName, String lastName, String pin) {
		
		User newUser = new User(firstName, lastName, pin, this);
		this.user.add(newUser);
		
		//Creating a new savings account for the user.
		Account newAccount = new Account("Savings", newUser, this);
		
		//add to holder and bank lists
				newUser.addAccount(this);
				this.addAccount(newAccount);
		
				return newUser;
	}
	
	public User userLogIn(String userID, String pin) {
		
		for (User u :this.users) {
			
			if (u.getid().compareTo(userID) == 0 && validatePin(pin)) {
				return.u
			}
		}
		
		return null;
	}
}

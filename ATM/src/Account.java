import java.util.ArrayList;

public class Account {

	private String name;
	private String id;
	private User holder;
	private ArrayList<Transaction> transactions;
	
	public Account(String name, User holder, Bank theBank) {
		this.name = name;
		this.holder = holder;
		this.id = theBank.getNewAccountID();
		this.transactions = new ArrayList<Transaction>();
		
		
	}
	public String getid() {
		return this.id;
	}
}
